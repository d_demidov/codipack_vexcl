#ifndef CODI_VEXCL_ADAPTOR_HPP
#define CODI_VEXCL_ADAPTOR_HPP

namespace codi {

//---------------------------------------------------------------------------
// vex::symbolic types do not contain values, so there is no way to check for
// equality to zero. Hence, the specialization of isTotalZero just return
// false.
//---------------------------------------------------------------------------
template <typename T>
struct IsTotalZeroImpl<T,
    typename std::enable_if<
        !std::is_arithmetic<T>::value &&
        std::is_same<
            typename boost::proto::domain_of<T>::type,
            vex::generator::symbolic_domain
            >::value
        >::type
    >
{
    static CODI_INLINE bool isTotalZero(const T &t) {
        return false;
    }
};

//---------------------------------------------------------------------------
// CoDiPack needs to take address of the 'real' type, and in the case of
// vex::symbolic this is not possible directly, since boost::proto overloads
// address_of operation and returns an expression instead.
//---------------------------------------------------------------------------
template <typename T>
struct addressof_impl<T,
    typename std::enable_if<
        std::is_same<
            typename boost::proto::domain_of<T>::type,
            vex::generator::symbolic_domain
            >::value
        >::type
    >
{
    typedef typename std::add_pointer<T>::type pointer_type;

    static inline pointer_type get(T &t) {
        return boost::addressof(t);
    }
};

//---------------------------------------------------------------------------
// Again, there is no way to tell if vex::symbolic is finite, since there is no
// value to check.
//---------------------------------------------------------------------------
template <typename T>
struct isfinite_impl<T,
    typename std::enable_if<
        !std::is_arithmetic<T>::value &&
        std::is_same<
            typename boost::proto::domain_of<T>::type,
            vex::generator::symbolic_domain
            >::value
        >::type
    >
{
    static inline bool get(const T &t) {
        return true;
    }
};

} // codi

#endif
