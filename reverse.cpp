#include <iostream>
#include <vexcl/vexcl.hpp>
#include <codi.hpp>

#include "codi_vexcl_adaptor.hpp"

typedef vex::symbolic<double> sym_real;

typedef codi::ActiveReal<
            codi::JacobiTape<
                codi::ChunkTapeTypes<
                    sym_real,
                    codi::LinearIndexHandler<int>
                    >
                >
            > VexReverse;

//---------------------------------------------------------------------------
int main() {
    vex::Context ctx(vex::Filter::Env);
    std::cout << ctx << std::endl;

    std::ostringstream body;
    vex::generator::set_recorder(body);

    VexReverse::TapeType &tape = VexReverse::getGlobalTape();
    VexReverse::TapeType fresh_tape;
    tape.swap(fresh_tape);

    sym_real x_s(sym_real::VectorParameter, sym_real::Const);
    sym_real g_s(sym_real::VectorParameter, sym_real::Const);
    sym_real d_s(sym_real::VectorParameter);

    VexReverse x(x_s);

    tape.setActive();
    tape.registerInput(x);

    VexReverse y = x * x * x;

    tape.registerOutput(y);
    tape.setPassive();

    y.setGradient(g_s);
    tape.evaluate();

    d_s = x.getGradient();

    auto kernel = vex::generator::build_kernel(ctx, "differentiate",
            body.str(), x_s, g_s, d_s);

    vex::vector<double> X(ctx, 4), G(ctx, 4), D(ctx, 4);
    X = 4.0;
    G = 1.0;

    kernel(X, G, D);

    std::cout << "df/dx = " << D << std::endl;
}

