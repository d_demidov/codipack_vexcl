#include <iostream>
#include <vexcl/vexcl.hpp>
#include <codi.hpp>

#include "codi_vexcl_adaptor.hpp"

namespace vex {
namespace sparse {

template <>
struct spmv_ops_impl<cl_double2, cl_double2> {
    // How to declare accumulator variable (s in s = sum(A_ij * x_j))
    static void decl_accum_var(backend::source_generator &src, const std::string &name)
    {
        src.new_line() << "double2 " << name << " = {0, 0};";
    }

    // How to append two values of type pack2:
    static void append(backend::source_generator &src,
            const std::string &sum, const std::string &val)
    {
        src.new_line() << sum << " codi_add(" << sum << ", " << val << ");";
    }

    // How to compute row product. Specifically, how to do (s += A_ij * x_j)
    // kind of operation.
    static void append_product(backend::source_generator &src,
            const std::string &sum, const std::string &mat_val, const std::string &vec_val)
    {
        src.new_line() << sum << " = codi_add(" << sum << ", codi_mul(" << mat_val << ", " << vec_val << "));";
    }
};

} // namespace sparse
} // namespace vex

typedef vex::symbolic<double> real;
typedef vex::symbolic<cl_double2> codi_real;

typedef codi::ActiveReal< codi::ForwardEvaluation<real> > VexRealForward;

VEX_FUNCTION(double, get_value,    (cl_double2, p), return p.x; );
VEX_FUNCTION(double, get_gradient, (cl_double2, p), return p.y; );
VEX_FUNCTION(cl_double2, make_codi, (double, v)(double, g),
        double2 r = {v, g};
        return r;
        );

struct codi_add {
    template <class Real>
    Real operator()(Real a, Real b) {
        VexRealForward A(get_value(a), get_gradient(a));
        VexRealForward B(get_value(b), get_gradient(b));

        VexRealForward C = A + B;
        return make_codi(C.getValue(), C.getGradient());
    }
};

struct codi_mul {
    template <class Real>
    Real operator()(Real a, Real b) {
        VexRealForward A(get_value(a), get_gradient(a));
        VexRealForward B(get_value(b), get_gradient(b));

        VexRealForward C = A * B;
        return make_codi(C.getValue(), C.getGradient());
    }
};

std::string header() {
    vex::backend::source_generator src;

    get_value.define(src);
    get_gradient.define(src);
    make_codi.define(src);

    auto add = vex::generator::make_function<cl_double2(cl_double2, cl_double2)>(codi_add());
    auto mul = vex::generator::make_function<cl_double2(cl_double2, cl_double2)>(codi_mul());

    add.define(src, "codi_add");
    mul.define(src, "codi_mul");

    return src.str();
}

int main(int nargs, char** args) {
    vex::Context ctx(vex::Filter::Env);
    std::cout << ctx << std::endl;

    vex::scoped_program_header h(ctx, header());

    int n = 5;
    std::vector<codi::RealForward> x; x.reserve(n);
    for(int i = 0; i < n; ++i) x.push_back(codi::RealForward(i, i==n/2));

    std::vector<int> ptr; ptr.reserve(n+1); ptr.push_back(0);
    std::vector<int> col; ptr.reserve(n*3);
    std::vector<cl_double2> val; val.reserve(n*3);

    for(int i = 0; i < n; ++i) {
        if (i == 0 || i == n - 1) {
            col.push_back(i);
            val.push_back(cl_double2{1, 0});
        } else {
            codi::RealForward cl = 0.1 * x[i-1];
            codi::RealForward cc = 5.0 * x[i];
            codi::RealForward cr = 0.1 * x[i+1];

            col.push_back(i-1);
            col.push_back(i);
            col.push_back(i+1);

            val.push_back(cl_double2{cl.getValue(), cl.getGradient()});
            val.push_back(cl_double2{cc.getValue(), cc.getGradient()});
            val.push_back(cl_double2{cr.getValue(), cr.getGradient()});
        }

        ptr.push_back(col.size());
    }

    vex::sparse::ell<cl_double2> A(ctx, n, n, ptr, col, val);

    vex::vector<cl_double2> X(ctx, n);
    vex::vector<cl_double2> Y(ctx, n);

    X = cl_double2{1, 0};

    Y = A * X;

    std::cout << Y << std::endl;
}
