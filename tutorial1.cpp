#include <boost/core/addressof.hpp>
#include <codi.hpp>
#include <iostream>
int main(int nargs, char** args) {
    codi::RealForward x(1.0, 0.0);
    codi::RealForward y(2.0, 1.0);

    codi::RealForward z = x * x + y * y;

    std::cout << "f(4.0) = " << z << std::endl;
    std::cout << "df/dx(4.0) = " << z.getGradient() << std::endl;
    return 0;
}
