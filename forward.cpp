#include <iostream>
#include <vexcl/vexcl.hpp>
#include <codi.hpp>

#include "codi_vexcl_adaptor.hpp"

typedef vex::symbolic<double> real;
typedef vex::symbolic<cl_double2> codi_real;

typedef codi::ActiveReal< codi::ForwardEvaluation<real> > VexForward;

VEX_FUNCTION(double, get_value,    (cl_double2, p), return p.x; );
VEX_FUNCTION(double, get_gradient, (cl_double2, p), return p.y; );
VEX_FUNCTION(cl_double2, make_codi, (double, v)(double, g),
        double2 r = {v, g};
        return r;
        );

struct codi_mul {
    template <class Real>
    Real operator()(Real a, Real b) {
        VexForward A(get_value(a), get_gradient(a));
        VexForward B(get_value(b), get_gradient(b));

        VexForward C = A * B;
        return make_codi(C.getValue(), C.getGradient());
    }
};

int main(int nargs, char** args) {
    vex::Context ctx(vex::Filter::Env);
    std::cout << ctx << std::endl;

    vex::backend::source_generator src;
    get_value.define(src);
    get_gradient.define(src);
    make_codi.define(src);

    vex::scoped_program_header h(ctx, src.str());

    auto mul = vex::generator::make_function<cl_double2(cl_double2, cl_double2)>(codi_mul());

    vex::vector<cl_double2> x(ctx, {cl_double2{2, 1}, cl_double2{2, 0}});
    vex::vector<cl_double2> y(ctx, {cl_double2{4, 0}, cl_double2{4, 1}});

    vex::vector<cl_double2> z(ctx, 2);
    z = mul(x, y);

    std::cout << z << std::endl;
}
