#include <boost/core/addressof.hpp>
#include <codi.hpp>
#include <iostream>

codi::RealReverse func(const codi::RealReverse& x) {
    return x * x * x;
}

int main(int nargs, char** args) {
    codi::RealReverse x = 4.0;
    codi::RealReverse::TapeType& tape = codi::RealReverse::getGlobalTape();

    tape.setActive();
    tape.registerInput(x);

    codi::RealReverse y = func(x);
    tape.registerOutput(y);
    tape.setPassive();
    y.setGradient(1.0);
    tape.evaluate();
    std::cout << "f(4.0) = " << y << std::endl;
    std::cout << "df/dx(4.0) = " << x.getGradient() << std::endl;
    return 0;
}
